import { HstPercy } from '@histoire/plugin-percy'
import { HstScreenshot } from '@histoire/plugin-screenshot'
import { HstVue } from '@histoire/plugin-vue'
import { defineConfig } from 'histoire'

export default defineConfig({
  plugins: [
    HstVue(),
    HstPercy({
      // Options here
    }),
    HstScreenshot({})
  ]
})
